#ifndef _lineDetector_tools

#define _lineDetector_tools
#include <opencv2/opencv.hpp>
#include <iostream>
#include <list>
#include <vector>
#include <math.h> 
#include <tuple>

using namespace std;
using namespace cv;

/// <summery>
/// This struct gets used to sort a list of points by its X axis.
/// </summery>
struct SortX
{
	inline bool operator()(const Point& point1, const Point& point2) const {
		return point1.x < point2.x;
	}
};



/// <summery>
/// Calculates the distance between two points.
/// </summery>
/// <param name="point1">
/// The first point.
/// </param>
/// <param name="point2">
/// The second point.
/// </param>
/// <returns> The distance between the two points. </returns>
float distance(Point point1, Point point2);

/// <summery>
/// This struct gets used to sort lists of points by the distance to the point in the constructor parameter.
/// </summery>
struct LessDistance
{
	Point mainPoint;

	LessDistance(Point point) {
		mainPoint = point;
	}

	inline bool operator()(const Point& point1, const Point& point2) const {
		return distance(mainPoint, point1) < distance(mainPoint, point2);
	}
};

/// <summery>
/// Calculates the white value between two points.
/// </summery>
/// <param name="point1">
/// The first point.
/// </param>
/// <param name="point2">
/// The second point.
/// </param>
/// <param name="img">
/// The binary image.
/// </param>
/// <returns> The mean white value between the two points. </returns>
float colorBetweenPoints(Point point1, Point point2, Mat img);

/// <summery>
/// Reduces a list of points to the k nearest points of one point.
/// </summery>
/// <param name="k">
/// The number of points that get returned.
/// </param>
/// <param name="point">
/// The reference point.
/// </param>
/// <param name="points">
/// The list of points.
/// </param>
/// <returns> List of points with the k nearest points to the reference point. </returns>
vector<Point> nearestNeighbors(int k, Point point, vector<Point> points);

/// <summery>
/// Reduces a list of points to the k nearest points of one point.
/// </summery>
/// <param name="currentLine">
/// The line that gets detected.
/// </param>
/// <param name="points">
/// All other points.
/// </param>
/// <param name="img">
/// The binary image.
/// </param>
/// <param name="whiteThreshold">
/// The minimum white value between points.
/// </param>
/// <param name="errorThreshold">
/// The maximum Error value of a line.
/// </param>
/// <returns> The next point for the current line. </returns>
Point FindBestCandidate(vector<Point> currentLine, vector<Point> points, Mat img, float whiteThreshold, float errorThreshold);

/// <summery>
/// This function calculates the error of this line.
/// </summery>
/// <param name="line">
/// The line.
/// </param>
/// <returns> The calculated line error. </returns>
float LineError(vector<Point> line);

/// <summery>
/// This function calculates the error of this line.
/// Adds a point to the line before the calculation.
/// </summery>
/// <param name="line">
/// The line.
/// </param>
/// <param name="point">
/// The new Point.
/// </param>
/// <returns> The calculated line error. </returns>
float LineError(vector<Point> line, Point point);

/// <summery>
/// Deletes a Point from a vector of points.
/// </summery>
/// <param name="point">
/// The point that shell get deleted from the vector.
/// </param>
/// <param name="vector">
/// The pointer to a Vector in wich the point gets deleted.
/// </param>
void deletePointFromVector(Point point, vector<Point> &vector);

/// <summery>
/// Deletes a line from a vector of lines.
/// </summery>
/// <param name="point">
/// The line that shell get deleted from the vector.
/// </param>
/// <param name="vector">
/// The pointer to a Vector in wich the line gets deleted.
/// </param>
void deleteLineFromVector(vector<Point> line, vector<vector<Point>>& vector);

#endif