#ifndef _lineDetector_crossing

#define _lineDetector_crossing

#include<opencv2/opencv.hpp>
#include<iostream>
#include <list>
#include <vector>
#include <math.h> 
#include <tuple>
#include "tools.h"
using namespace std;
using namespace cv;

#ifndef M_PI
#define M_PI	3.14159265358979323846f
#endif

#define MIN_WHITE 150
#define MIN_ANGLE 5
#define LENGTH_PERCENT 0.1f

enum crossingSide { front, middle, back };
enum crossingType { L, X, T };
struct Crossing
{
	Point crossingPoint;
	crossingType type;
	vector<Point> line1;
	crossingSide side1;

	vector<Point> line2;
	crossingSide side2;
};

/// <summery>
/// This function calculates the intersection point of the two points. 
/// </summery>
/// <param name="line1">
/// The first line.
/// </param>
/// <param name="line2">
/// The second line.
/// </param>
/// <returns> The intersection Point </returns>
Point LineIntersection(vector<Point> line1, vector<Point> line2);

/// <summery>
/// This function checks if the point is on the image.
/// </summery>
/// <param name="point">
/// The Point that needs to get checked.
/// </param>
/// <param name="img">
/// The image.
/// </param>
/// <returns> True if the point is on the image. </returns>
bool IsPointOnImage(Point point, Mat img);

/// <summery>
/// This function checks if the point is on the image.
/// </summery>
/// <param name="point">
/// The Point that needs to get checked.
/// </param>
/// <param name="img">
/// The image.
/// </param>
/// <returns> True if the point is on the image. </returns>
Point ClosestToPoint(vector<Point> line, Point point);

/// <summery>
/// Connect all lines that belong connected together.
/// </summery>
/// <param name="lines">
/// The lines that need to get checked and than connected.
/// </param>
/// <param name="minAngle">
/// The Angle to check if two lines are straight length.
/// </param>
/// <param name="minWhite">
/// The minimum white value between the points of the two lines.
/// </param>
/// <param name="img">
/// The binary image.
/// </param>
/// <returns> List of lines with the lines connected. </returns>
vector<vector<Point>> ConnectExtentionOfLines(vector<vector<Point>> lines, float minEngle, float minWhite, Mat img);

/// <summery>
/// Calculates the white level between the crossing Point and the line end Points.
/// </summery>
/// <param name="line1">
/// The first line.
/// </param>
/// <param name="line2">
/// The second line.
/// </param>
/// <param name="crossing">
/// The crossing point.
/// </param>
/// <param name="img">
/// The binary image.
/// </param>
/// <returns> the white value between the crossing point and the line. </returns>
float WhiteLevelOnCrossing(vector<Point> line1, vector<Point> line2, Point crossing, Mat img);

/// <summery>
/// Calculates the white level between the crossing Point and the line end Points.
/// </summery>
/// <param name="line1">
/// The first line.
/// </param>
/// <param name="line2">
/// The second line.
/// </param>
/// <param name="img">
/// The binary image.
/// </param>
/// <returns> the white value between the crossing point and the line. </returns>
float WhiteLevelOnCrossing(vector<Point> line1, vector<Point> line2, Mat img);

/// <summery>
/// Calculates the Angle between two lines.
/// </summery>
/// <param name="line1">
/// The first line.
/// </param>
/// <param name="line2">
/// The second line.
/// </param>
/// <returns> The Angle between the two lines. </returns>
float GetAngle(vector<Point> line1, vector<Point> line2);

/// <summery>
/// Checks on whitch side of the line the crossing point is.
/// </summery>
/// <param name="line">
/// The line.
/// </param>
/// <param name="crossing">
/// The crossing point.
/// </param>
/// <param name="edgePercent">
/// The percent of the line that counts as corner.
/// </param>
/// <returns> The crossing side. </returns>
crossingSide CheckCrossingSide(vector<Point> line, Point crossing, float edgePercent);

/// <summery>
/// Checks the crossing is from type T, X, or L.
/// </summery>
/// <param name="line1">
/// The first line.
/// </param>
/// <param name="line2">
/// The second line.
/// </param>
/// <param name="crossing">
/// The crossing point.
/// </param>
/// <param name="edgePercent">
/// The percent of the line that counts as corner.
/// </param>
/// <returns> Returns the crossing type. </returns>
crossingType GetCrossingType(vector<Point> line1, vector<Point> line2, Point crossing, float edgePercent);

/// <summery>
/// Checks for all crossings in the list of lines and returns the crossings with position, the lines, and the type.
/// </summery>
/// <param name="lines">
/// The list of lines.
/// </param>
/// <param name="minAngle">
/// The minimal Angle between the lines to be a crossing.
/// </param>
/// <param name="minWhite">
/// THe minimal White value between the lines and the crossing.
/// </param>
/// <param name="edgePercent">
/// The percent of the line that counts as corner.
/// </param>
/// <param name="img">
/// The binary image.
/// </param>
/// <returns> Returns all crossing. </returns>
vector<Crossing> GetAllCrossings(vector<vector<Point>> lines, float minAngle, float minWhite, float edgePercent, Mat img);

/// <summery>
/// Checks for all crossings in the list of lines and returns the crossings with position, the lines, and the type.
/// This method uses the defined values as threasholds.
/// </summery>
/// <param name="lines">
/// The list of lines.
/// </param>
/// <param name="img">
/// The binary image.
/// </param>
/// <returns> Returns all crossing. </returns>
vector<Crossing> GetAllCrossings(vector<vector<Point>> lines, Mat img);

#endif