#ifndef _lineDetector_lines

#define _lineDetector_lines

#include<opencv2/opencv.hpp>
#include<iostream>
#include <list>
#include <vector>
#include <math.h> 
#include <tuple>
#include "tools.h"
using namespace std;
using namespace cv;

#define LINE_DICTANCE 12
#define LINE_THREASHOLD 187
#define LINE_MIN_WHITE 200
#define LINE_ERROR_THREASHOLD 3

/// <summery>
/// This function makes a binary image out of the image in the parameters.
/// It turns it into a gray scale image and uses the defined threashold.
/// </summery>
/// <param name="img">
/// The image that gets transformed into a binary image.
/// </param>
/// <returns> A binary image </returns>
Mat ImageBinarization(Mat img);

/// <summery>
/// This function detected points on the lines in a binary image.
/// It uses the LINE_DICTANCE to get the dictance between the lines that get scaned for points.
/// </summery>
/// <param name="img">
/// A binary image.
/// </param>
/// <returns> Points in the middel of the lines in the binary image. </returns>
vector<Point> DetectLinePoints(Mat img);

/// <summery>
/// This function detected lines.
/// The line error of the detected lines dose not go over the threashold in LINE_ERROR_THREASHOLD.
/// The minimum white value between the points in a line dose not go over the LINE_MIN_WHITE value.
/// </summery>
/// <param name="img">
/// A binary image.
/// </param>
/// <param name="detectedPoints">
/// Points on the lines.
/// </param>
/// <returns> Detected lines. </returns>
vector<vector<Point>> DetectLinesFromPoints(Mat img, vector<Point> detectedPoints);

/// <summery>
/// This function detected lines.
/// The line error of the detected lines dose not go over the threashold in LINE_ERROR_THREASHOLD.
/// The minimum white value between the points in a line dose not go over the LINE_MIN_WHITE value.
/// </summery>
/// <param name="img">
/// A binary image.
/// </param>
/// <returns> Detected lines. </returns>
vector<vector<Point>> DetectLines(Mat img);
#endif