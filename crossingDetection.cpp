#include "crossingDetection.h"

Point LineIntersection(vector<Point> line1, vector<Point> line2)
{
	float x1 = line1.front().x;
	float x2 = line1.back().x;
	float y1 = line1.front().y;
	float y2 = line1.back().y;

	float x3 = line2.front().x;
	float x4 = line2.back().x;
	float y3 = line2.front().y;
	float y4 = line2.back().y;

	float Px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
	float Py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
	return Point((int)Px, (int)Py);
}

bool IsPointOnImage(Point point, Mat img) {
	return point.x < img.cols && point.y < img.rows;
}

Point ClosestToPoint(vector<Point> line, Point point) {
	std::sort(line.begin(), line.end(), LessDistance(point));
	return line.at(0);
}

float WhiteLevelOnCrossing(vector<Point> line1, vector<Point> line2, Point crossing, Mat img)
{
	if (!IsPointOnImage(crossing, img))
		return 0.0f;

	Point nearestPoint1 = ClosestToPoint(line1, crossing);
	Point nearestPoint2 = ClosestToPoint(line2, crossing);
	float color1 = colorBetweenPoints(crossing, nearestPoint1, img);
	float color2 = colorBetweenPoints(crossing, nearestPoint2, img);
	float color = (color1 + color2) / 2;

	return color;
}
float WhiteLevelOnCrossing(vector<Point> line1, vector<Point> line2, Mat img)
{
	return WhiteLevelOnCrossing(line1, line2, LineIntersection(line1, line2), img);
}

float PercentToLength(vector<Point> line, float edgePercent) {
	float length = distance(line.front(), line.back());
	return edgePercent * length;
}

float GetAngle(vector<Point> line1, vector<Point> line2) {
	double x1 = (float)line1.front().x - line1.back().x;
	double y1 = (float)line1.front().y - line1.back().y;
	double x2 = (float)line2.front().x - line2.back().x;
	double y2 = (float)line2.front().y - line2.back().y;
	double angle1, angle2, angle;

	if (x1 != 0.0f)
		angle1 = atan(y1 / x1);
	else
		angle1 = 3.14159 / 2.0; // 90 degrees
	if (x2 != 0.0f)
		angle2 = atan(y2 / x2);
	else
		angle2 = 3.14159 / 2.0; // 90 degrees
	angle = fabs(angle2 - angle1);
	angle = angle * 180.0 / 3.14159; // convert to degrees 

	return angle;
}
bool IsConnected(vector<Point> line1, vector<Point> line2, float minWhite, Mat img) {
	return WhiteLevelOnCrossing(line1, line2, img) > minWhite;
}

bool IsCorner(vector<Point> line1, vector<Point> line2, float minEngle, float minWhite, Mat img) {
	if (GetAngle(line1, line2) > minEngle)
		return IsConnected(line1, line2, minWhite, img);
	else
		return false;
}

bool IsExtentionOfLine(vector<Point> line1, vector<Point> line2, float minEngle, float minWhite, Mat img) {
	if (GetAngle(line1, line2) < minEngle)
		return IsConnected(line1, line2, minWhite, img);
	else
		return false;
}

vector<vector<Point>> ConnectExtentionOfLines(vector<vector<Point>> lines, float minEngle, float minWhite, Mat img) {
	vector<vector<Point>> newLines;
	while (lines.size() != 0) {
		newLines.push_back(lines.front());
		lines.erase(lines.begin());

		vector<vector<Point>> linesTodelete;

		for (vector<Point> line : lines) {
			if (IsExtentionOfLine(line, newLines.back(), minEngle, minWhite, img)) {
				vector<Point> line1 = newLines.back();
				newLines.pop_back();
				for (Point point : line) {
					line1.push_back(point);
				}
				std::sort(line1.begin(), line1.end(), SortX());
				newLines.push_back(line1);
				linesTodelete.push_back(line);
			}
		}
		for (vector<Point> line : linesTodelete)
			deleteLineFromVector(line, lines);

	}
	return newLines;
}

crossingSide CheckCrossingSide(vector<Point> line, Point crossing, float edgePercent) {
	float edgeLength = PercentToLength(line, edgePercent);
	float lineLength = distance(line.front(), line.back());
	float distanceFrontCrossing = distance(line.front(), crossing);
	float distanceBackCrossing = distance(line.back(), crossing);

	if (distanceFrontCrossing > lineLength && distanceFrontCrossing > distanceBackCrossing) {
		return back;
	}
	else if (distanceBackCrossing > lineLength && distanceBackCrossing > distanceFrontCrossing) {
		return front;
	}
	else if (distanceFrontCrossing < edgeLength) {
		return front;
	}
	else if (distanceBackCrossing < edgeLength) {
		return back;
	}
	else {
		return middle;
	}
}

crossingType GetCrossingType(vector<Point> line1, vector<Point> line2, Point crossing, float edgePercent) {
	crossingSide sideLine1 = CheckCrossingSide(line1, crossing, edgePercent);
	crossingSide sideLine2 = CheckCrossingSide(line2, crossing, edgePercent);
	if (((sideLine1 == middle) && ((sideLine2 == front) || (sideLine2 == back))) || ((sideLine2 == middle) && ((sideLine1 == front) || (sideLine1 == back))))
		return T;
	if (((sideLine2 == front) || (sideLine2 == back)) && ((sideLine1 == front) || (sideLine1 == back)))
		return L;
	if ((sideLine1 == middle) && (sideLine2 == middle))
		return X;
}

vector<Crossing> GetAllCrossings(vector<vector<Point>> lines, float minEngle, float minWhite, float edgePercent, Mat img) {
	lines = ConnectExtentionOfLines(lines, minEngle, minWhite, img);

	vector<Crossing> crossings;

	while (lines.size() != 0) {
		vector<Point> thisLine = lines.back();
		lines.pop_back();

		for (vector<Point> nextLine : lines) {
			Point crossingPoint = LineIntersection(thisLine, nextLine);

			if (!IsPointOnImage(crossingPoint, img) || !IsCorner(thisLine, nextLine, minEngle, minWhite, img)) {
				continue;
			}
			Crossing crossing;
			crossing.crossingPoint = crossingPoint;
			crossing.type = GetCrossingType(thisLine, nextLine, crossingPoint, edgePercent);
			crossing.line1 = thisLine;
			crossing.side1 = CheckCrossingSide(thisLine, crossingPoint, edgePercent);
			crossing.line2 = nextLine;
			crossing.side2 = CheckCrossingSide(nextLine, crossingPoint, edgePercent);
			crossings.push_back(crossing);
		}
	}
	return crossings;
}

vector<Crossing> GetAllCrossings(vector<vector<Point>> lines, Mat img) {
	return GetAllCrossings(lines, MIN_ANGLE, MIN_WHITE, LENGTH_PERCENT, img);
}


