#include "tools.h"

float distance(Point point1, Point point2) {
	return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
}

float distancePointLine(Point point, Point line1, Point line2) {
	if (line1.x == line2.x && line1.y == line2.y) {
		return distance(point, line1);
	}
	if (line1.x == line2.x) {
		return abs(line1.x - point.x);
	}
	else if (line1.y == line2.y) {
		return abs(line1.y - point.y);
	}

	float y = line1.y - line2.y;
	float x = line1.x - line2.x;

	float m = y / x;
	float b = line1.y - m * line1.x;

	float m2 = -1 / m;
	float b2 = point.y - m2 * point.x;

	Point cross((b2 - b) / (m - m2), m2 * ((b2 - b) / (m - m2)) + b2);
	return distance(point, cross);
}

float colorBetweenPoints(Point point1, Point point2, Mat img) {
	LineIterator line(img, point1, point2);
	list<int> colors;
	for (int i = 0; i < line.count; i++, line++) {
		Point point = line.pos();
		colors.push_back(img.at<uchar>(point));
	}

	int color = 0;
	for (int thisColor : colors)
	{
		color += thisColor;
	}
	return color / colors.size();
}

vector<Point> nearestNeighbors(int k, Point point, vector<Point> points) {
	std::sort(points.begin(), points.end(), LessDistance(point));
	vector<Point> neighbors;
	for (int i = 0; i < k && i < points.size(); i++) {
		neighbors.push_back(points.at(i));
	}
	return neighbors;
}

float LineError(vector<Point> line) {
	float sum = 0;
	if (line.size() < 3) {
		return 0;
	}
	for (int i = 1; i < line.size() - 1; i++)
	{
		sum += distancePointLine(line.at(i), line.front(), line.back());
	}
	sum = sum / (line.size() - 2);
	return sum;
}

float LineError(vector<Point> line, Point point) {
	line.push_back(point);
	return LineError(line);
}

Point FindBestCandidate(vector<Point> currentLine, vector<Point> points, Mat img, float whiteThreshold,  float errorThreshold) {

	vector<Point> fiveNearest = nearestNeighbors(5, currentLine.back(), points);

	for (Point neighbor : fiveNearest) {
		cout << "white: " << colorBetweenPoints(currentLine.back(), neighbor, img) << endl;

		if (colorBetweenPoints(currentLine.back(), neighbor, img) > whiteThreshold && LineError(currentLine, neighbor) < errorThreshold) {
			return neighbor;
		}
	}
	throw "No Candidate";

	/*vector<Point> fiveNearest = nearestWhiteNeighbors(5, point, points, img);

	Point whitest;

	
	for (int i = 0; i < fiveNearest.size(); i++) {
		int whiteness = colorBetweenPoints(point, fiveNearest.at(i), img);
		if (whiteness < 250) {
			fiveNearest.erase(fiveNearest.begin() + i);
		}
	}
	if (fiveNearest.size() == 0) {
		throw "No Candidate";
	}

	for (int i = 0; i < fiveNearest.size(); i++) {
		if (i == 0) {
			whitest = fiveNearest.at(i);
		}
		else if (colorBetweenPoints(whitest, point, img) < colorBetweenPoints(fiveNearest.at(i), point, img)
			|| (colorBetweenPoints(whitest, point, img) == colorBetweenPoints(fiveNearest.at(i), point, img)
				&& distance(whitest, point) > distance(fiveNearest.at(i), point))) {
			whitest = fiveNearest.at(i);
		}
	}
	return whitest;*/
}



void deletePointFromVector(Point point, vector<Point> &vector) {
	for (int i = 0; i < vector.size(); i++) {
		if (point.x == vector.at(i).x && point.y == vector.at(i).y) {
			vector.erase(vector.begin() + i);
		}
	}
}

void deleteLineFromVector(vector<Point> line, vector<vector<Point>>& vector) {
	for (int i = 0; i < vector.size(); i++) {
		if (line == vector.at(i)) {
			vector.erase(vector.begin() + i);
		}
	}
}
