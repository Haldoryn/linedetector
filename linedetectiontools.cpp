#include "linedetectiontools.h"

Mat ImageBinarization(Mat img) {
	Mat img_gray;
	cvtColor(img, img_gray, COLOR_BGR2GRAY);
	Mat out;
	threshold(img_gray, out, LINE_THREASHOLD, 255, 0);
	return out;
}

vector<Point> DetectLinePoints(Mat img) {
	vector<Point> dots;
	for (int col = 0; col < img.cols; col += LINE_DICTANCE)
	{
		vector<Point> whitePoints;
		for (int row = 0; row < img.rows; row++)
		{
			int color = img.at<uchar>(Point(col, row));
			if (color != 0) {
				whitePoints.push_back(Point(col, row));
			}
			else if (whitePoints.size() != 0) {
				dots.push_back(whitePoints.at((int)(whitePoints.size() / 2)));
				whitePoints.clear();
			}
		}
	}
	for (int row = 0; row < img.rows; row += LINE_DICTANCE)
	{
		vector<Point> whitePoints;
		for (int col = 0; col < img.cols; col++)
		{
			int color = img.at<uchar>(Point(col, row));
			if (color != 0) {
				whitePoints.push_back(Point(col, row));
			}
			else if (whitePoints.size() != 0) {
				dots.push_back(whitePoints.at((int)(whitePoints.size() / 2)));
				whitePoints.clear();
			}
		}
	}
	std::sort(dots.begin(), dots.end(), SortX());
	dots.erase(unique(dots.begin(), dots.end()), dots.end());
	return dots;
}

vector<vector<Point>> DetectLinesFromPoints(Mat img, vector<Point> detectedPoints) {
	std::sort(detectedPoints.begin(), detectedPoints.end(), SortX());
	vector<vector<Point>> lines;
	vector<Point> line; //queue. first and last is start and end of the line

	line.push_back(detectedPoints.front());
	detectedPoints.erase(detectedPoints.begin());

	while (detectedPoints.size() != 0) {

		try {
			Point carnidate = FindBestCandidate(line, detectedPoints, img, LINE_MIN_WHITE, LINE_ERROR_THREASHOLD);
			line.push_back(carnidate);
			deletePointFromVector(carnidate, detectedPoints);
		}
		catch (const char* msg)
		{
			if (line.size() > 1)
				lines.push_back(line);
			line.clear();
			line.push_back(detectedPoints.front());//New points for the line gets pushed from the list of dots and deleted from the list of dots
			detectedPoints.erase(detectedPoints.begin());
		}
	}
	if (line.size() > 1)
		lines.push_back(line);
	return lines;
}

vector<vector<Point>> DetectLines(Mat img) {
	vector<Point> detectedPoints = DetectLinePoints(img);
	return DetectLinesFromPoints(img, detectedPoints);
}
