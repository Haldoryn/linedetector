#include "drawing.h"

void drawPoints(vector<Point> points, Mat& img, Scalar color, int thickness)
{
	for (Point point : points)
		drawPoint(point, img, color, thickness);
}

void drawPoint(Point point, Mat& img, Scalar color, int thickness)
{
	circle(img, point, 0, color, thickness);
}

void drawCrossing(Crossing crossing, Mat& img, Scalar colorPoint, Scalar colorText) {
	string text;
	drawPoint(crossing.crossingPoint, img, colorPoint, 10);

	if (crossing.type == L) {
		text = "L";
	}
	if (crossing.type == X) {
		text = "X";
	}
	if (crossing.type == T) {
		text = "T";
	}
	putText(img, text, Point(crossing.crossingPoint.x - 3, crossing.crossingPoint.y + 3), FONT_HERSHEY_COMPLEX_SMALL, 0.5, colorText, 1);

}

void drawCrossings(vector<Crossing> crossings, Mat& img, Scalar colorPoint, Scalar colorText) {
	for (Crossing crossing : crossings) {
		drawCrossing(crossing, img, Scalar(255, 0, 0), Scalar(0, 0, 255));
	}
}


void drawLines(vector<vector<Point>> lines, Mat& img, Scalar color, int thickness)
{
	for (vector<Point> line : lines)
		drawLine(line, img, color, thickness);
}

void drawLine(vector<Point> thisline, Mat& img, Scalar color, int thickness)
{
	line(img, thisline.front(), thisline.back(), color, thickness);
}