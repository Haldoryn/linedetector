#ifndef _lineDetector_draw

#define _lineDetector_draw
#include <opencv2/opencv.hpp>
#include <iostream>
#include <list>
#include <vector>
#include <math.h> 
#include <tuple>
#include "crossingDetection.h"

using namespace std;
using namespace cv;

/// <summery>
/// Draws a list of points to an image.
/// </summery>
/// <param name="points">
/// A vector of points that get drawn on an image.
/// </param>
/// <param name="img">
/// The pointer to an BGR image in wich the points get drawn.
/// </param>
/// <param name="color">
/// The color of the drawing.
/// </param>
/// <param name="thickness">
/// The thickness of the drawing.
/// </param>
void drawPoints(vector<Point> points, Mat& img, Scalar color, int thickness = 1);

/// <summery>
/// Draws a point to an image.
/// </summery>
/// <param name="point">
/// The point that get drawn on an image.
/// </param>
/// <param name="img">
/// The pointer to an BGR image in wich the point get drawn.
/// </param>
/// <param name="color">
/// The color of the drawing.
/// </param>
/// <param name="thickness">
/// The thickness of the drawing.
/// </param>
void drawPoint(Point point, Mat& img, Scalar color, int thickness = 1);

/// <summery>
/// Draws a crossing to an image with a dot and a letter resembling the crossing type.
/// </summery>
/// <param name="crossing">
/// A crossing that get drawn on an image.
/// </param>
/// <param name="img">
/// The pointer to an BGR image in wich the crossing gets drawn.
/// </param>
/// <param name="colorPoint">
/// The color of the point.
/// </param>
/// <param name="colorText">
/// The color of the text.
/// </param>
void drawCrossing(Crossing crossing, Mat& img, Scalar colorPoint, Scalar colorText);

/// <summery>
/// Draws a list of crossings to an image with a dot and a letter resembling the crossing type.
/// </summery>
/// <param name="crossings">
/// A vector of crossings that get drawn on an image.
/// </param>
/// <param name="img">
/// The pointer to an BGR image in wich the crossings get drawn.
/// </param>
/// <param name="colorPoint">
/// The color of the point.
/// </param>
/// <param name="colorText">
/// The color of the text.
/// </param>
void drawCrossings(vector<Crossing> crossings, Mat& img, Scalar colorPoint, Scalar colorText);

/// <summery>
/// Draws a list of lines to an image.
/// </summery>
/// <param name="lines">
/// A vector of lines that get drawn to a image.
/// </param>
/// <param name="img">
/// The pointer to an BGR image in wich the lines get drawn.
/// </param>
/// <param name="color">
/// The color of the drawing.
/// </param>
/// <param name="thickness">
/// The thickness of the drawing.
/// </param>
void drawLines(vector<vector<Point>> lines, Mat& img, Scalar color, int thickness = 1);

/// <summery>
/// Draws a line to an image.
/// </summery>
/// <param name="line">
/// A line that get drawn to a image.
/// </param>
/// <param name="img">
/// The pointer to an BGR image in wich the line get drawn.
/// </param>
/// <param name="color">
/// The color of the drawing.
/// </param>
/// <param name="thickness">
/// The thickness of the drawing.
/// </param>
void drawLine(vector<Point> thisline, Mat& img, Scalar color, int thickness = 1);

#endif