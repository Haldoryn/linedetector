#include<opencv2/opencv.hpp>
#include<iostream>
#include <list>
#include <vector>
#include <math.h> 
#include <tuple>
#include "crossingDetection.h"
#include "tools.h"
#include "linedetectiontools.h"
#include "drawing.h"
using namespace std;
using namespace cv;

int main()
{
	string imagePath = "image.jpg";
	Mat img = imread(imagePath);

	if (img.empty())
	{
		cout << "can not open " << imagePath << endl;
		return -1;
	}


	Mat img_bin = ImageBinarization(img);
	vector<vector<Point>> lines = DetectLines(img_bin);
	vector<Crossing> crossings = GetAllCrossings(lines, img_bin);

	drawLines(lines, img, Scalar(0, 255, 0));
	drawCrossings(crossings, img, Scalar(0, 0, 255), Scalar(255, 255, 255));
	imshow("Image", img);

	waitKey(0);
	return 0;
}

